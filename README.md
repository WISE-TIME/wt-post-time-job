## Description

This repository will be used by Jenkins seed job to create scheduled pipeline.

Created pipeline will create long-pooling connection to Wisetime servers to receive posted timelogs. When teammember post time - it will be delegated to WT agent
though this pipeline to integrate with third party services.

Required job param is AGENT_TYPE (e.g. jira, invoice-demo etc). It will be set by seed job. If you need to import with job manually be sure to set it.

This job expects agent to be preinstalled in JENKINS_HOME directory.

Job will be interrupted if not finished in 1437 minutes (1 day minus 3 minutes).
